print("Zadanie 5a")

def main():
    dlugosc=30
    my_list = list(range(2,dlugosc))
    w = 2
    while w < dlugosc:
        my_list = list(filter(lambda arg: (arg % w or arg == w), my_list))
        w += 1
    #print(my_list)
    print(list(map(lambda arg:arg**2, my_list)))

if __name__ == '__main__':
    main()