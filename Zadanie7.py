print("zadanie 7")
from functools import wraps

def my_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Funkcja ma nazwe '+  func.__name__ + " o nastepujacych argumentach:" )
        return func(*args,**kwargs)
    return wrapper

@my_decorator

def g(*args, **kwargs):
    for el in args:
        print('args: '+str(el))
    for key, value in kwargs.items():
        print('kwargs: '+'{0}={1}'.format(key,value))

def main():
    g(1,3,4,5,a='6',b='7')

if __name__ == '__main__':
        main()