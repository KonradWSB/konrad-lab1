print("Zadanie 5b")

def main():
    dlugosc=30
    my_list = list(range(2,dlugosc))
    w = 2
    while w < dlugosc:
        my_list = list(filter(lambda arg: (arg % w or arg == w), my_list))
        w += 1
    #print(my_list)
    print(list(el**2 for el in my_list))

if __name__ == '__main__':
    main()