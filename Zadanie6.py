print("Zadanie 6")

def my_gen(N):
    el=0
    el2=1
    for i in range(N):
        yield el
        el, el2 = el2, el + el2

def main():
    obj=my_gen(20)
    seq = list(obj)
    print(seq)

if __name__ == '__main__':
    main()