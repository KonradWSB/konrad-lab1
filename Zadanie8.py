print("zadanie 8")
from functools import wraps

def my_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        wrapper.calls +=1
        return func(*args,**kwargs)
    wrapper.calls = 0
    return wrapper

@my_decorator
def g(*args, **kwargs):
    for el in args:
        print('args: '+str(el))
    for key, value in kwargs.items():
        print('kwargs: '+'{0}={1}'.format(key,value))
    print("funkcje wywolano po raz " + str(g.calls) + ".")

def main():
    g(1,2,3)
    g(4,5,6)
    print("funkcje wywolano " + str(g.calls) + " razy")

if __name__ == '__main__':
        main()
